package pairwiseltd.enron

import scala.Ordering

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.storage.StorageLevel

object EnronDataAnalysis {
  val HELP = "help"
  val TEST = "-t"
  val USAGE =
    NEWLINE +
      "Syntax" + NEWLINE +
      List(TAB + "<run off_heap_size> <file_split_partitions> <enron_data_path> [" + TEST + "]",
        "Options",
        TAB + "off_heap_size: Tungsten off heap memory size eg 1g",
        TAB + "file_split_partitions: wholeTextFiles Spark Api minPartition argument. Tune this " +
          "according to your number of executors in your cluster as described in README file",
        TAB + "enron_data_path: local, HDFS or S3 path to enron data",
        TAB + "[" + TEST + "]: Option to indicate testing environment",
        "Spark Environment Example",
        TAB + "spark-submit --driver-memory 2g --executor-memory 4g --driver-cores 4 " +
          "--num-executors 16 /home/hadoop/enron_2.11-0.1.0-SNAPSHOT.jar 10g 64 s3n://enron-bucket/enron-data/**/*.txt").
        mkString(NEWLINE + NEWLINE) +
        NEWLINE

  def main(args: Array[String]): Unit =
    {
      // TODO use an argument parser library
      if (args.length == 1 && args(0).equalsIgnoreCase(HELP)) System.err.print(USAGE)
      else {
        require(args.length > 2, USAGE)
        val isTest = args.length == 4 && args(3).equalsIgnoreCase(TEST)
        val offHeapSize = args(0)
        val fileSplitPartitions = args(1).toInt
        val enronFilesDir = args(2)
        val conf = new SparkConf()
          .setAppName("Enron Analysis")
          .set("spark.memory.offHeap.size", offHeapSize)
          .set("spark.memory.offHeap.enabled", "true")
        // using builtin Tungsten columnar off-heap store and prevent GC pauses on persisted partitions 
        val sc = new SparkContext(
          if (isTest) conf.setMaster("local[*]").set("spark.executor.memory", "450g")
          else conf) // set executor and memory settings from spark-submit
        try {
          // TRANSFORMATIONS:
          val rddFileNameContent = sc.wholeTextFiles(enronFilesDir,
            fileSplitPartitions)

          val rddEnronEmails = rddFileNameContent.mapValues {
            content =>
              {
                val email = EnronData.parse(content)
                email match {
                  case Some(x) => {
                    val combinedTupleAddr =
                      x.stats.receipientTo.map(entry => (entry, 2L)) ::: x.stats.recipientCc.map(entry => (entry, 1L))
                    (x.emailId, combinedTupleAddr, x.stats.wordCount)
                  }
                  case None => ("", Nil, -1)
                }
              }
          }.filter {
            case (_, (_, _, wordCount)) => wordCount != -1
          }

          // TODO first shuffling! But eliminating duplicated emails
          val rddForRecipientsWithDuplicatedEmail = (rddEnronEmails.map {
            case (partitionId, (emailId, recipients, wordCount)) => (emailId, (recipients, wordCount))
          }).persist(StorageLevel.OFF_HEAP)

          val rddForRecipientsWithUnduplicatedEmail =
            rddForRecipientsWithDuplicatedEmail.reduceByKey((value1, value2) => value1)

          val rddForWordCount = (rddForRecipientsWithUnduplicatedEmail mapValues {
            case ( _, wordCount) => wordCount
          }).persist(StorageLevel.OFF_HEAP)

          // TODO second shuffling! 
          val rddForRecipients =
            rddForRecipientsWithUnduplicatedEmail.flatMap {
              case (key, (recipients, wc)) => (recipients)
            }.persist(StorageLevel.OFF_HEAP)

          val rddForRecipientsReduced = rddForRecipients.reduceByKey {
            case (occ1, occ2) => occ1 + occ2
          }

          // ACTIONS!
          val (emailCount, wordCount) = timed {
            rddForWordCount.aggregate((0L, 0L))(
              { case ((emailSum, wordSum), (emailId, wordCount)) => (emailSum + 1, wordSum + wordCount) },
              (r, l) => (r._1 + l._1, r._2 + l._2))
          }

          val averageWordCount = wordCount / emailCount;
          // TODO saveAsTextFile
          println(s"averageWordCount = $averageWordCount")

          val first100 = timed {
            rddForRecipientsReduced.top(100)(Ordering[Long].on {
              case (address, count) => count
            })
          }
          // TODO saveAsTextFile
          first100.foreach(p => println(s"${p._1} ${p._2}"))

        } finally {
          sc.stop()
        }
      }
    }
}
