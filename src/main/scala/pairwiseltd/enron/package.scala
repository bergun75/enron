package pairwiseltd

package object enron {
  val DATE_TOKEN: String = "Date: "
  val FROM_TOKEN: String = "From: "
  val TO_TOKEN: String = "To: "
  val CC_TOKEN: String = "Cc: "
  val DOC_ID_TOKEN: String = "X-SDOC: "
  val EML_ID_TOKEN: String = "X-ZLID: "
  val ORIGINAL_MSG_TOKEN: String = "-----Original Message-----"
  val NEWLINE: String = "\n"
  val TAB: String = "\t"
  val ENRON_STARS_TOKEN: String = "***********"
  val WHITESPACES: String = "\\s"
  val EMPTY_STRING: String = ""
  val ENRON_INFO_TOKEN: String = "EDRM Enron Email Data Set has been produced"
  val ENRON_EMAIL_SEPARATOR: String = ","
  val LINE_SEPARATOR: String = "|"
  val WORD_SEPARATOR: String = "\\W+"
  val DATE_FROM_SEPARATOR = "_"
  val IDX_DOES_NOT_EXIST: Int = -1

  /**
   * Executes its function argument with elapsed time information
   *
   *  @param block function taht will be executed within the timed block
   *  @return R original return type of the block function
   */
  def timed[R](block: => R): R = {
    val t0 = System.nanoTime()
    val result = block // call-by-name
    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) / 1000000 + "ms")
    result
  }
}