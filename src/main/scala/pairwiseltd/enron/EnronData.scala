package pairwiseltd.enron

import scala.util.Failure
import scala.util.Success
import scala.util.Try

case class EnronEmail(emailId: String,
                      stats: EnronStats)
case class EnronStats(receipientTo: List[String],
                      recipientCc: List[String],
                      wordCount: Int)

case class EnronEmailCoords(dateBeginIdx: Int, dateEndIdx: Int,
                            fromBeginIdx: Int, fromEndIdx: Int,
                            toBeginIdx: Int, toEndIdx: Int,
                            ccBeginIdx: Int, ccEndIdx: Int,
                            messageBeginIdx: Int, messageEndIdx: Int)

/** Factory for enron data parsing. */
object EnronData {
  /**
   * Parses content of a file provided as String
   *
   * @param content file content as String
   *
   * @return Some[EnronEmail] if parsed properly otherwise None.
   */
  private[enron] def parse(content: String): Option[EnronEmail] =
    {
      // NOTE avoiding NFA regex pattern matching because of non linear time gurantee!
      // I could have written this functionality simpler with pattern matching
      // Though here I prefered linear text search approach to favor performance
      // which makes code somewhat uglier
      for {
        textCoords <- parseEnronEmail(content)
        enronMail <- parseEnronEmailCoords(content)(textCoords)
      } yield enronMail
    }

  // TODO below private functions must be refactored in a util class or some
  // better design pattern and written specs to 
  private[this] def parseEnronEmailCoords(content: String)(
    emailTxtCoord: EnronEmailCoords): Option[EnronEmail] = {
    val res = Try {
      val dateStr =
        content.substring(emailTxtCoord.dateBeginIdx,
          emailTxtCoord.dateEndIdx).trim
      val fromStr =
        extractEmailAddress(content.substring(emailTxtCoord.fromBeginIdx,
          emailTxtCoord.fromEndIdx))
      val toEmails =
        splitEmailAddresses(content.substring(emailTxtCoord.toBeginIdx,
          emailTxtCoord.toEndIdx))

      val ccEmails =
        if (emailTxtCoord.ccBeginIdx == -1) { List[String]() }
        else {
          splitEmailAddresses(content.substring(emailTxtCoord.ccBeginIdx,
            emailTxtCoord.ccEndIdx))
        }
      val messageWordCount =
        if (emailTxtCoord.messageEndIdx > emailTxtCoord.messageBeginIdx) {
          content.substring(emailTxtCoord.messageBeginIdx,
            emailTxtCoord.messageEndIdx).split(WORD_SEPARATOR).size
        } else { 0 }
      Some(EnronEmail(dateStr + DATE_FROM_SEPARATOR + fromStr,
        EnronStats(
          toEmails,
          ccEmails,
          messageWordCount)))
    }
    res match {
      case Success(x) => x
      case Failure(_) => {
        // TODO log this
        None
      }
    }
  }

  private[this] def splitEmailAddresses(adressStr: String): List[String] = {
    val addresses = adressStr.split(ENRON_EMAIL_SEPARATOR) map {
      entry =>
        {
          extractEmailAddress(entry)
        }
    }
    addresses.toList
  }

  private[this] def extractEmailAddress(address: String): String = {
    val idxLeft = address.indexOf("<")
    val idxRight = address.indexOf(">")
    val email = if (idxLeft == -1) {
      address.toLowerCase
    } else {
      address.substring(idxLeft + 1, idxRight).toLowerCase
    }
    email.trim
  }

  private[this] def parseEnronEmail(content: String): Option[EnronEmailCoords] = {
    for {
      dateBeginIdx <- findIdx(content, DATE_TOKEN, 0)
      dateEndIdx <- findIdx(content, NEWLINE, dateBeginIdx)
      fromBeginIdx <- findIdx(content, FROM_TOKEN, dateEndIdx)
      fromEndIdx <- findIdx(content, NEWLINE, fromBeginIdx)
      toBeginIdx <- findIdx(content, TO_TOKEN, fromEndIdx)
      toEndIdx <- findIdxSpecial(content, NEWLINE, toBeginIdx)
      docIdBeginIdx <- findIdx(content, DOC_ID_TOKEN, toBeginIdx)
      ccBeginIdx <- findIdx(content.take(docIdBeginIdx), CC_TOKEN, toBeginIdx, true)
      emlIdBeginIdx <- findIdx(content, EML_ID_TOKEN, docIdBeginIdx)
      messageBeginIdx <- findIdx(content, NEWLINE, emlIdBeginIdx)
      orgMsgIdx <- findIdx(content, ORIGINAL_MSG_TOKEN, emlIdBeginIdx, true)
      enronMessageBeginIdx <- findIdx(content, ENRON_INFO_TOKEN, messageBeginIdx)
      messageEndIdx <- if (orgMsgIdx == -1) {
        Some(enronMessageBeginIdx - ENRON_INFO_TOKEN.size - ENRON_STARS_TOKEN.size - 1)
      } else {
        Some(orgMsgIdx - ORIGINAL_MSG_TOKEN.size - 1)
      }
      ccEndIdx <- if (ccBeginIdx == IDX_DOES_NOT_EXIST) {
        Some(-1)
      } else {
        findIdxSpecial(content, NEWLINE, ccBeginIdx)
      }
      if (toBeginIdx < docIdBeginIdx &&
        docIdBeginIdx < messageBeginIdx)

    } yield (EnronEmailCoords(
      dateBeginIdx, dateEndIdx,
      fromBeginIdx, fromEndIdx,
      toBeginIdx, toEndIdx,
      ccBeginIdx, ccEndIdx,
      messageBeginIdx, messageEndIdx))

  }

  private[this] def findIdx(content: String,
                            token: String,
                            from: Int,
                            cont: Boolean = false): Option[Int] = {
    val contentIdx = content.indexOf(token, from)
    if (contentIdx > -1) {
      val tokenIdx = contentIdx + token.size
      Some(tokenIdx)
    } else if (cont) {
      Some(-1)
    } else { None }
  }

  // was necessary because of ugly formating in To and Cc fields
  private[this] def findIdxSpecial(content: String,
                                   token: String,
                                   from: Int,
                                   cont: Boolean = false): Option[Int] = {
    findIdx(content, token, from, cont) match {
      case Some(x) => {
        val char = content.charAt(x + 1)
        if (char.isWhitespace) {
          findIdxSpecial(content, token, x, cont)
        } else {
          Some(x)
        }
      }
      case None => None
    }
  }
}

