package pairwiseltd.enron

import scala.io.Source

import org.scalatest.Matchers
import org.scalatest.WordSpec
import org.scalatest.prop.TableDrivenPropertyChecks

trait TestData extends TableDrivenPropertyChecks {
  val fileWithCC: String = "src/test/resources/enron/3.869600.POWQBE2VCTANL0U1ZPD2Q2APMIXJ24ERA.txt"
  val fileWithNothing: String = "src/test/resources/enron/3.869396.JAA4TTKNJ0UJMM1W0PTPJD5R1Y4STIKNA.1.txt"  
  val fileWithoutCC: String = "src/test/resources/enron/3.869259.PYEL0IQMXCXTHOIIL1BPMW1BHOQ3UUBMA.txt"

  val examples = Table(("inputFile", "expectedOutput"),
    (fileWithCC, Some(EnronEmail("Mon, 19 Nov 2001 15:38:59 -0800 (PST)" + DATE_FROM_SEPARATOR + "greg.porter@enron.com",
      EnronStats(
        List("mary.kay.miller@enron.com",
          "bill.rapp@enron.com",
          "maria.pavlou@enron.com"),
        List("robert.kilmer@enron.com",
          "fkelly@gbmdc.com",
          "mark_mcconnell@enron.net",
          "steven.harris@enron.com",
          "steve.kirk@enron.com"), 7)))),
    (fileWithNothing, None),    
    (fileWithoutCC, Some(EnronEmail(
        "Fri, 28 Dec 2001 13:07:36 -0800 (PST)" + DATE_FROM_SEPARATOR + "glen.hass@enron.com", EnronStats(
      List("bill.rapp@enron.com"),
      List(), 67)))))

}

class EnronDataSpec extends WordSpec
    with Matchers
    with TestData {

  forAll(examples) { (inputFile, expectedOutput) =>

    "EnronData parsing" when {
      s"$inputFile is provided" should {
        s"have output as $expectedOutput" in {
          val content = Source.fromFile(inputFile).getLines.mkString("\n")
          val actualOutput = EnronData.parse(content)
          actualOutput shouldEqual expectedOutput
        }
      }
    }
  }
}