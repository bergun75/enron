PROBLEM
-------

Provided snapshot of Enron data is available in a pretty unstructured and unevenly sized fashion. 
Such as there are some really large files which are 3.7G and some others 5M of zip files found together.
The zip files has v1 and v2 of the required data. I will be concentrating on version2 of the data
which I found more structured to work with. v2 zip files contain and xml list of emails in the list.
This xml file contains the DOCID, To and Cc fields which we are interested in but is missing the 
number of words contained in the message body of the original email mesage. The count of words can
be extracted from each email text file found under text_000 folder. The overall size of the data
is huge! say hundreds of Gig.

ASSUMPTIONS AND DECISIONS:
-------------------------

1-) All information we require such as (To, Cc, word count) is available from text versions of email
messages under text_000 folder. I will use these documents only.

2-) The tokens from common package.scala would be used to parse each email text file. Most important ones
being:

val DATE_TOKEN: String = "Date: "
val FROM_TOKEN: String = "From: "
val TO_TOKEN: String = "To: "
val CC_TOKEN: String = "Cc: "
val DOC_ID_TOKEN: String = "X-SDOC: "
val EML_ID_TOKEN: String = "X-ZLID: "
val ORIGINAL_MSG_TOKEN: String = "-----Original Message-----"
val ENRON_STARS_TOKEN: String = "***********"
val ENRON_INFO_TOKEN: String = "EDRM Enron Email Data Set has been produced"

3-) In order to avoid process the same email and avoid duplication I have used a combination of 
DATE_TOKEN and FROM_TOKEN to uniquely identify each email message. I have discovered DOC_ID_TOKEN
is not a unique identifier for duplicated emails. Although this introduces me some more shuffling
and performance bottleneck, this saves like 8 minutes compared to initial job and yields more meaningful
results!

4-) When processing a reciepient he/she has more than 1 unique identifier such as name.surname@enron.com
initial.surname@enron.com or only name surname. So at the moment I am treating each of this ids
separately; in the future this can be improved with a mapper of identities to solve the problem.

5-) A defensive parsing strategy is used and malformed or non email(attachment) text files
are ignored. NFA Regex pattern matching is avoided in order to prevent non deterministic time complexities
on parsing file content.
Top 100 reciepients: (Please read Assumption 4)
6-) Although the attachments are ignored by the parser I strongly suggest that those files
must be omited from the target directory. Because large files and their retrieval through
network will cause huge network latencies and we are not really interested with that part of the
data. Publicly available snap-06ad5685a891271e9 EBS snapshot prepared by me provides this data.

7-) As previously declared only the email text files will be stored on S3 storage and they will
be made available under directories with equal file numbers. Since average email text file size
is around 10K if we assume we have 600000 emails that will boil down to 6G of storage needed.
For the sake of this exercise I will store them as raw text files as opposed to some hadoop 
friendly compressed formats.

8-) Storing emails as in original format in unevenly sized small to large zip files will cause huge
network latencies. Trying to stream information from zip file is going to be expensive because
of its sequenced nature and vast amount of unneeded data in all off the zip files.

9-) The number of the email files to be found on each S3 sub directory will be calculated in parallel to
minPartitions setting of the SparkContext wholeFileText data retreival API. This will be described
in later section. This will be one of the most important tuning exercise.

10-) Since the transformations don't require complex joins or any other fancy transformation chain
I didn't feel like to use Spark SQL although I prefered to use the Off Heap serializer as 
Spark SQL does. Spark SQL could do further optimizations though I wanted to be in control of 
every optimization and create a reference point spark job that could be evolved later.

11-) I do the word count only on the message body. And I assume message body starts from line below
"X-ZLID: ......." and ends before ORIGINAL_MSG_TOKEN or ENRON_INFO_TOKEN line. So I don't count
the words in other forwarded mail text bodies.


MEETING AND PREPARING PRECONDITIONS:
-----------------------------------

As described above we will preprocess the enron dataset to extract the information we need only. But
before doing that we need to take some measures and make some calculations in order to come up with
how many sub-directories we need in our S3 bucket for evenly sized email text collections. This is
done in order to tune wholeTextFile API function of Spark. According to my experiences if we provide
wholeTextFile a path with a subdirectory count which is a multiple of its minPartitions argument than
spark will more evenly distribute the files between its workers and partitions.

For this purpose because of the time limitations; I have not followed the instructions described in
"More Detailed Calculations" and took a simpler approach. I created the snapshot-id EBS volume 
with 12 partitioned folders only. 

This is publicly available with snap-06ad5685a891271e9. These folders are formed with pretty equal number 
of files inside them. This can further be changed to have equal size of file content in the future.

* More Detailed Calculations: (Skip this part if bored.)

Because of this we must know the configuration of our spark environment in terms of workers and available 
nodes. minPartitions recommendation is to use 2 or 3 times number of available workers on the node. I will
take available workers as 3 in our exercise.

For example:

If we have 3 worker node in our spark cluster and each worker node has 8 CPU cores than

3*8*3 = 72 we will use this for minPartitions argument.

Than the number of subdirectories of emails under S3 bucket must be 72 * x. Lets try to find x
If we want 1000 email texts in each subdirectory and we have a total of 600000 email text files.

x = 600000 / (72*1000) =~ 8 (rounded)
number of desired subdirectories = 8*72 = 576

So we will tailor a script which will extract only email files say files under text000 folder
which has an email file name convention which is like 

"3.869468.BJ40KZXRZPQTUMROKIHZ4AABKEKBH2NJA.txt"

attachments file naming convention is different:

3.869468.BJ40KZXRZPQTUMROKIHZ4AABKEKBH2NJA.1.txt

The ending local directory would be around 6GB of raw text emails considering 600000 emails.

Next step will be deciding number of files per subdirectory.

600000 / 576 =~ 1041

Prepare your aws cli script to upload every 1041 file to a new directory like 0000 increasing the
counter on the subdirectory name.

You should end up with directories under your S3 bucket such as

s3:/enron-bucket/enron-data/0000/
s3:/enron-bucket/enron-data/0001/
...
s3:/enron-bucket/enron-data/0576/

with around 1041 file in each one.

This way wholeTextFile API would create more balanced partitions across your Spark cluster.

Challenge: If you want to increase your worker nodes and end up with new executor counts than
you "might" need to change your S3 directory structure for better performance. If you want to keep
the same dataset and make different calculations over the time; I suggest taking into account
"the number of executors per node" only, when calculating the needed number of subdirectories 
on s3 bucket. This is essential for wholeTextFile to evenly partition your data.

Following this advice will help you achieve more parallelism when executing your Spark Jobs by
providing more partitions.



USAGE:
-----------------------------------
Syntax
  run <off_heap_size> <file_split_partitions> <enron_data_path> [-t]

Options

  off_heap_size: Tungsten off heap memory size eg 1g

  file_split_partitions: wholeTextFiles Spark Api minPartition argument. Tune this according to your number of executors in your cluster as described in this README file

  enron_data_path: local, HDFS or S3 path to enron data

  [-t]: Option to indicate testing environment

Spark Environment Example

  spark-submit --driver-memory 1g --executor-memory 1g --driver-cores 4 --num-executors 24 /home/hadoop/enron_2.11-0.1.0-SNAPSHOT.jar 10g 72 s3n://enron-bucket/enron-data/**/*.txt

NOTE: copy this spark application to your master node in Spark cluster and run with the above spark-submit script as "hadoop" user as client mode. Or if you prefer to run in cluster mode
it is upto you.

BUILD ENVIRONMENT:
-----------------------------------------
sbt
sbt test
sbt package 


RESULTS:
-----------------------------------------
Environment: Default AWS EMR Spark instance with 1 master 2 slaves.
The first action took 6410738ms which is ~ 106 minutes. The second action took 8 seconds only. And below are the results:

*averageWordCount = 192 (Please read Assumption 11)*

*Top 100 reciepients: (Please read Assumption 4)*

pete.davis@enron.com 11213
tana jones 10042
jeff dasovich 7701
mark taylor 7648
sara shackleton 7336
louise.kitchen@enron.com 7264
richard shapiro 6682
james d steffes 6097
steven j kean 5699
jeff.dasovich@enron.com 5137
vince j kaminski 5068
susan j mara 4551
james.d.steffes@enron.com 4465
sara.shackleton@enron.com 4157
bill.williams.iii@enron.com 4131
daren j farmer 4014
paul kaufman 3982
kimberly.watson@enron.com 3933
richard.shapiro@enron.com 3622
gerald.nemec@enron.com 3608
jdasovic@enron.com 3607
sally beck 3603
gerald nemec 3398
mark.taylor@enron.com 3261
william s bradford 3244
vince.j.kaminski@enron.com 3235
susan bailey 3228
klay@enron.com 3203
sally.beck@enron.com 3186
sandra mccubbin 3179
michelle.lokay@enron.com 3127
mark.guzman@enron.com 3119
kenneth.lay@enron.com 3114
tim belden 3111
john.j.lavorato@enron.com 3084
greg.whalley@enron.com 2871
kam.keiser@enron.com 2863
vkaminski@aol.com 2862
tana.jones@enron.com 2861
rick.buy@enron.com 2829
joe hartsoe 2815
carol st clair 2795
kate symes 2764
kay mann 2721
lynn.blair@enron.com 2709
kay.mann@enron.com 2702
harry kingerski 2691
darrell.schoolcraft@enron.com 2689
ryan.slinger@enron.com 2663
stacey.w.white@enron.com 2662
karen denne 2659
kevin.m.presto@enron.com 2644
craig.dean@enron.com 2616
jeffrey t hodge 2597
bert.meyers@enron.com 2587
richard b sanders 2576
tim.belden@enron.com 2562
elizabeth sager 2559
mark e haedicke 2547
geir.solberg@enron.com 2495
shelley.corman@enron.com 2485
alan comnes 2416
karen lambert 2382
tom moran 2375
chris.germany@enron.com 2372
taylor 2346
brent hendry 2341
daren.j.farmer@enron.com 2337
williams 2330
smith 2320
lorraine.lindberg@enron.com 2308
steve.kean@enron.com 2250
elizabeth.sager@enron.com 2244
stephanie panus 2230
sarah novosel 2218
skean@enron.com 2210
mark palmer 2207
jeffrey.a.shankman@enron.com 2196
don.baughman@enron.com 2177
tk.lohman@enron.com 2145
mary cook 2144
phillip.m.love@enron.com 2138
jeff.skilling@enron.com 2137
lindy.donoho@enron.com 2120
samantha boyd 2119
frank l davis 2097
steven.harris@enron.com 2088
andy.zipper@enron.com 2087
greg whalley 2036
paul.y'barbo@enron.com 2027
miller 2016
scott.neal@enron.com 2002
john j lavorato 1985
brant reves 1978
mike.maggi@enron.com 1977
rod.hayslett@enron.com 1956
monika.causholli@enron.com 1956
mary hain 1936
mona l petrochko 1915
joe.parks@enron.com 1913

FURTHER IMPROVEMENTS:
-----------------------------------------

1-) Spark SQL can be tried and compared with this referential initial Spark Job

2-) Spark Graphx can be made part of the solution such as email addresses can become vertices
and each email sent to other address can be represented with directional edges as To: represented
with 2 edge and Cc: represented with 1 edge. Finally we can select the top 100 by their indegree
values.

3-) Partitioning of files on S3 can be increased and partition folders can be arranged to have equal
bytes of data vs equal counts of files.
